from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from typing import List, Optional
from uuid import uuid4


app = FastAPI()

# front end rodando liveserver do vscode
origins = ['http://localhost:5500']

app.add_middleware(
	CORSMiddleware,
	allow_origins=origins,
	allow_credentials=True,
	allow_methods=["*"],
	allow_headers=["*"]
)

@app.get('/')
def read_root(nome: str):
	return f'Bem-Vindo de volta, {nome}'

@app.get('/animais')
def listaDeAnimais(animais: str):
	animal = ['pato', 'gato']
	return f'[{animal}]'

class Animais(BaseModel):
	id:Optional[str]
	name: str
	age: int
	sexy: str
	color: str



@app.post('/pets')
def create_pets(animal: Animais):
	animal.id = str(uuid4())
	banco.append(animal)
	return f'{animal} cadastrado'


@app.get('/animais/{animal_id}')
def obter_animal(animal_id: str):
	for animal in banco:
		if animal.id == animal_id:
			return animal
	try:
		print(animal)
	except Error:
		print('Falha', Error)
	return animal_id

@app.delete('/animais/{animal_id}')
def remover_animal(animal_id):
	posicao = -1
	for indice, animal in enumerate(banco):
		if animal.id == animal_id:
			posicao = indice
			break
	if posicao != - 1:
		banco.pop(posicao)
		return {'msg': 'Excluido com sucesso'}
	else:
		return {'msg': 'Falha'}

banco: List[Animais] = []

@app.get('/pets')
def pets():
	return banco
