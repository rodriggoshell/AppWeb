async function CarregarAnimais() {
  // axios api para conectar o front com o back
  const  response = await axios.get('http://localhost:8000/pets')

  const animais = response.data;

  const lista = document.querySelector('#lista-animais');

  lista.innerHTML = '';

  animais.forEach(animal => {
    const items = document.createElement('li');
    items.innerText = animal.name;
    lista.appendChild(items)
  });
}

function EnviarDados() {
  const form_animal = document.querySelector('#form-animal');
  const input_name = document.querySelector('.texto');

  form_animal.onsubmit = async (e) => {
    e.preventDefault();
    const name_animal = input_name.value;

   await  axios.post('http://localhost:8000/pets', {
      name: name_animal,
      age: 3,
      sexy: 'F',
      color: 'black'
    });

    alert(`${name_animal} Cadastrado`);
    CarregarAnimais();
  }
}

const teste = document.querySelector('#excluir');
teste.addEventListener('click', () => {
  console.log('aqui rodou lindo!!');
})

function Excluir() {

}

function App() {
  console.log('Passou, ok');
  CarregarAnimais()
  EnviarDados()
  //DeletarDados()
}

App()
